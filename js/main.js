$('.right-menu-img').on("click", function() {
	$(".right-menu").css('display', 'block');
	$(".right-menu-img").css('display', 'none');
	$("html,body").css("overflow","hidden");
	$(".opacity").css("opacity", "0.5");
	// $(".opacity").css("pointer-events", "none");
	$(".fos-right").css("opacity", "0.5");
	// $(".fos-right").css("pointer-events", "none");
	$(".fos-left").css("opacity", "0.5");
	// $(".fos-left").css("pointer-events", "none");
});

$('.portfolio-item-img').on("click", function() {
	$(".pop").css("display", "block");
	$(".opacity").css("opacity", "0.2");
	$("html,body").css("overflow","hidden");
});

$('.menu-close').on("click", function() {
	$(".right-menu").css('display', 'none');
	$(".right-menu-img").css('display', 'block');
	$("html,body").css("overflow","hidden");
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
	$(".fos-right").css("opacity", "1");
	$(".fos-right").css("pointer-events", "auto");
	$(".fos-left").css("opacity", "1");
	$(".fos-left").css("pointer-events", "auto");
});

$('.nav').on("click", function() {
	$(".right-menu").css('display', 'none');
	$(".right-menu-img").css('display', 'block');
	$("html,body").css("overflow","hidden");
	$(".opacity").css("opacity", "1");
	// $(".opacity").css("pointer-events", "auto");
	$(".fos-right").css("opacity", "1");
	// $(".fos-right").css("pointer-events", "auto");
	$(".fos-left").css("opacity", "1");
	// $(".fos-left").css("pointer-events", "auto");
});

if (screen.width <= 1000) {
	$(".first_page").removeClass("mainpages");
	$(".main").removeClass("section");
	$(".about").removeClass("section");
	$(".about_team").removeClass("section");
	$(".portfolio").removeClass("section");
	$(".fos").removeClass("section");
	$(".services").removeClass("section");
	$(".about_min").removeClass("section");
};

if (screen.width > 1000) {
	$(document).ready(function(){
	      $(".mainpages").onepage_scroll({
	        sectionContainer: "section",
	        responsiveFallback: 600,
	        direction: "horizontal",
	        easing: "ease",
	        loop: true,
	        keyboard: true,
	        pagination: false
	      });
	});
};




$('.service-1-btn').on("click", function() {
	$(".service-1-btn").addClass("btn_active");
	$(".service-2-btn").removeClass("btn_active");
	$(".service-3-btn").removeClass("btn_active");
	$(".service-1").addClass("active");
	$(".service-2").removeClass("active");
	$(".service-3").removeClass("active");
	$(".service-1-btn").css("background-image", "url('img/text_w.svg')");
	$(".service-2-btn").css("background-image", "url('img/internet.svg')");
	$(".service-3-btn").css("background-image", "url('img/design.svg')");
});

$('.service-2-btn').on("click", function() {
	$(".service-2-btn").addClass("btn_active");
	$(".service-1-btn").removeClass("btn_active");
	$(".service-3-btn").removeClass("btn_active");
	$(".service-2").addClass("active");
	$(".service-1").removeClass("active");
	$(".service-3").removeClass("active");
	$(".service-1-btn").css("background-image", "url('img/text.svg')");
	$(".service-2-btn").css("background-image", "url('img/internet_w.svg')");
	$(".service-3-btn").css("background-image", "url('img/design.svg')");
});

$('.service-3-btn').on("click", function() {
	$(".service-3-btn").addClass("btn_active");
	$(".service-2-btn").removeClass("btn_active");
	$(".service-1-btn").removeClass("btn_active");
	$(".service-3").addClass("active");
	$(".service-2").removeClass("active");
	$(".service-1").removeClass("active");
	$(".service-1-btn").css("background-image", "url('img/text.svg')");
	$(".service-2-btn").css("background-image", "url('img/internet.svg')");
	$(".service-3-btn").css("background-image", "url('img/design_w.svg')");
});

// $('.submit-btn').on("click", function() {
// 	$(".zakaz").css("display", "block");
// 	$(".opacity").css("opacity", "0.2");
// 	$(".opacity").css("pointer-events", "none");
// 	$(".zakaz").css("position", "fixed");
// 	$("html,body").css("overflow","hidden");
// });

// $('.zzz').on("click", function() {
// 	$(".zakaz1").css("display", "block");
// 	$(".opacity").css("opacity", "0.2");
// 	$(".opacity").css("pointer-events", "none");
// 	$(".zakaz1").css("position", "fixed");
// 	$("html,body").css("overflow","hidden");
// });

$('.fa-times').on("click", function() {
	$(".zakaz").css("display", "none");
	$(".zakaz1").css("display", "none");
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
	$("html,body").css("overflow","hidden");
	$(".pop").css("display", "none");
});

$('.opacity').on("click", function() {
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
	$("html,body").css("overflow","hidden");
	$(".right-menu").css('display', 'none');
	$(".right-menu-img").css('display', 'block');
	$(".fos-right").css("opacity", "1");
	$(".fos-right").css("pointer-events", "auto");
	$(".fos-left").css("opacity", "1");
	$(".fos-left").css("pointer-events", "auto");
	$(".zakaz").hide();
    $(".zakaz1").hide();
});

$('.fos-left').on("click", function() {
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
	$("html,body").css("overflow","hidden");
	$(".right-menu").css('display', 'none');
	$(".right-menu-img").css('display', 'block');
	$(".fos-right").css("opacity", "1");
	$(".fos-right").css("pointer-events", "auto");
	$(".fos-left").css("opacity", "1");
	$(".fos-left").css("pointer-events", "auto");
	$(".zakaz").hide();
    $(".zakaz1").hide();
});

$('.fos-right').on("click", function() {
	$("html,body").css("overflow","hidden");
	$(".right-menu").css('display', 'none');
	$(".right-menu-img").css('display', 'block');
	$(".fos-right").css("opacity", "1");
	$(".fos-right").css("pointer-events", "auto");
	$(".fos-left").css("opacity", "1");
	$(".fos-left").css("pointer-events", "auto");
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
	$(".zakaz").hide();
    $(".zakaz1").hide();
});

document.body.onload = function() {
	setTimeout(function() {
		var preloader = document.getElementById('preloader');
		if (!preloader.classList.contains('done')) {
			preloader.classList.add('done');
		}
	}, 1000);
};


// $('.team_ng').on("mouseover", function() {
// 	
// 	// $('.about_team').css("filter", "brightness(50%)");
// });

// $('.team_ng').on("mouseleave", function() {
// 	$('.about_team').css("background-image", "none");
// 	$('.dark-screen').animate({'opacity': 1}, 400);
// 	// $('.about_team').css("filter", "brightness(100%)");
// });
$('.team_ng').hover(function() {
	/* Stuff to do when the mouse enters the element */
	$('.dark-screen').animate({'opacity': 0}, 500);
	$('.about_team').css("background-image", "url('img/ng_example.jpg')");
	
	$('.team-name').css('opacity', '1');
}, function() {
	/* Stuff to do when the mouse leaves the element */
	$('.about_team').css("background-image", "none");
	$('.dark-screen').animate({'opacity': 1}, 0);
	$('.team-name').css('opacity', '1');
});

if (screen.width > 800) {
$(document).ready(function(){
	$('.slider_about').slick({
		dots: true,
  		infinite: true,
  		speed: 600,
  		slidesToShow: 1,
  		autoplay: false,
  		prevArrow: '<button class="btn-next-left"></button>',
		nextArrow: '<button class="btn-next-right"></button>'
	});
});
};

if (screen.width <= 800) {
$(document).ready(function(){
	$('.slider_about').slick({
  		infinite: true,
  		speed: 600,
  		slidesToShow: 1,
  		autoplay: false
	});
});
};

$(document).ready(function(){
	$('#team_car').slick({
		dots: false,
  		infinite: true,
  		speed: 600,
  		slidesToShow: 1,
  		autoplay: true,
  		autoplaySpeed: 2000,
 		arrows: true
	});
});

$(document).ready(function(){
	$('#port_car').slick({
		dots: true,
  		infinite: true,
  		speed: 600,
  		slidesToShow: 1,
  		autoplay: true,
  		autoplaySpeed: 2000,
 		arrows: true
	});
});

// $(function(){
//   $('a[href^="#"]').on('click', function(event) {
//     event.preventDefault();
    
//     var sc = $(this).attr("href"),
//         dn = $(sc).offset().top;
    
//     $('html, body').animate({scrollTop: dn}, 1000);
    
//   });
// });


 $(document).ready(function(){ 
    	PopUpHide();
    });
    function PopUpShow(){
        $(".zakaz").show();
        $(".zakaz1").show();
        $(".opacity").css("opacity", "0.2");
        $("body").css("overflow","hidden");
    }
    //Функция скрытия PopUp
    function PopUpHide(){
        $(".zakaz").hide();
        $(".opacity").css("opacity", "1");
        $(".zakaz1").hide();
        $("html,body").css("overflow","hidden");
    }
















